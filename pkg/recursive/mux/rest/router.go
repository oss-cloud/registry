package rest

import (
	"net/http"

	"github.com/gorilla/mux"
)

type Mount func(*mux.Router)

func newMount(
	repositoryListHandler *repositoryListHandler,
	repositoryCreateHandler *repositoryCreateHandler,
) Mount {
	return func(router *mux.Router) {
		router.Path("/v1/repository").Methods(http.MethodGet).Handler(repositoryListHandler)
		router.Path("/v1/repository").Methods(http.MethodPost).Handler(repositoryCreateHandler)
	}
}
