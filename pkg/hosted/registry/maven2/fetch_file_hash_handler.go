package maven2

import (
	"net/http"
	"strings"

	"gitlab.com/oss-cloud/registry/pkg/hosted/registry/maven2/internal/repository"
	"gitlab.com/oss-cloud/registry/pkg/hosted/registry/maven2/internal/storage"

	"gitlab.com/oss-cloud/registry/pkg/common/logging"

	"github.com/gorilla/mux"
	spec "github.com/opencontainers/image-spec/specs-go/v1"
	"go.uber.org/zap"

	"gitlab.com/oss-cloud/registry/pkg/common/httphelper"
	"gitlab.com/oss-cloud/registry/pkg/remote"
)

var _ http.Handler = &fetchFileHashHandler{}

type fetchFileHashHandler struct {
	Remote *remote.Remote
	logger *zap.Logger
}

func newFetchFileHashHandler(remote *remote.Remote) *fetchFileHashHandler {
	return &fetchFileHashHandler{
		Remote: remote,
		logger: logging.RootLogger.Named("registry/maven/fetchFileHashHandler"),
	}
}

func (h *fetchFileHashHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	path := r.Context().Value(mavenPathContextKey).(repository.MavenPath)
	registryName := storage.ToRegistryName(vars["repository"], path.Coordinate)
	version := path.Coordinate.Version
	if version == "" {
		version = "index"
	}

	logger := h.logger.With(
		zap.String("registryName", registryName),
		zap.String("ref", version),
	)

	manifest, err := h.Remote.FetchManifest(r.Context(), registryName, version)
	if err != nil {
		logger.Error("failed to fetch manifest", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if manifest == nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	var descriptor spec.Descriptor

	if version == "index" {
		descriptor = manifest.Layers[0]
	} else {
		found := remote.FindFile(manifest.Layers, strings.TrimSuffix(path.Filename, "."+path.HashType.String()))
		if found == nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		descriptor = *found
	}

	content, ok := descriptor.Annotations[path.HashType.String()]
	if !ok {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	header := w.Header()
	header.Set("Etag", httphelper.DigestToEtag(descriptor.Digest))
	header.Set("Content-Type", "text/plain")
	w.Write([]byte(content))
}
