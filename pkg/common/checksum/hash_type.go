package checksum

import (
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"hash"
)

type HashType string

var HashTypeValues = []HashType{
	HashTypeMD5,
	HashTypeSHA1,
	HashTypeSHA256,
	HashTypeSHA512,
}

const (
	HashTypeMD5    HashType = "md5"
	HashTypeSHA1   HashType = "sha1"
	HashTypeSHA256 HashType = "sha256"
	HashTypeSHA512 HashType = "sha512"
)

func (h HashType) String() string {
	return string(h)
}

func (h HashType) Hash() hash.Hash {
	switch h {
	case HashTypeMD5:
		return md5.New()
	case HashTypeSHA1:
		return sha1.New()
	case HashTypeSHA256:
		return sha256.New()
	case HashTypeSHA512:
		return sha512.New()
	default:
		return nil
	}
}
