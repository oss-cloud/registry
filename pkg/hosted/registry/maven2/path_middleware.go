package maven2

import (
	"net/http"
	"strings"

	"github.com/gorilla/mux"

	"gitlab.com/oss-cloud/registry/pkg/common/context"
	"gitlab.com/oss-cloud/registry/pkg/hosted/registry/maven2/internal/repository"
)

var mavenPathContextKey = context.Key("hosted/maven/path")

func mavenPathMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		repo := vars["repository"]
		prefix := "/registry/" + repo + "/maven/"
		mavenPath := strings.TrimPrefix(r.URL.Path, prefix)
		path := repository.ParseMavenPath(mavenPath)
		ctx := context.WithValue(r.Context(), mavenPathContextKey, path)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
