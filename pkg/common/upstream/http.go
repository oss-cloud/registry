package upstream

import (
	"context"
	"io"
	"net/http"

	"go.uber.org/zap"

	"gitlab.com/oss-cloud/registry/pkg/common/errs"
	"gitlab.com/oss-cloud/registry/pkg/common/logging"
)

type Client struct {
	Client *http.Client
	logger *zap.Logger
}

func NewClient(client *http.Client) *Client {
	return &Client{
		Client: client,
		logger: logging.RootLogger.Named("common/upstream/client"),
	}
}

func (c *Client) Fetch(ctx context.Context, registryPrefix, packageName, etag string) (content io.ReadCloser, latestETag string, err error) {
	req, err := http.NewRequest(http.MethodGet, registryPrefix+"/"+packageName, nil)
	if err != nil {
		c.logger.Error(
			"error when build request to upstream",
			zap.String("packageName", packageName),
			zap.String("upstream", registryPrefix),
			zap.Error(err),
		)
		return
	}

	if etag != "" {
		req.Header.Set("If-None-Match", etag)
	}

	resp, err := c.Client.Do(req.WithContext(ctx))
	if err != nil {
		c.logger.Error(
			"error when request to upstream",
			zap.String("packageName", packageName),
			zap.String("upstream", registryPrefix),
			zap.Error(err),
		)
		return
	}

	switch resp.StatusCode {
	case http.StatusNotFound:
		resp.Body.Close()
		return nil, "", nil
	case http.StatusNotModified:
		resp.Body.Close()
		return nil, resp.Header.Get("ETag"), nil
	case http.StatusOK:
		return resp.Body, resp.Header.Get("ETag"), nil
	default:
		return nil, "", errs.ErrUnexpectedStatus
	}
}
