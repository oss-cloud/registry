package core

import (
	"github.com/google/uuid"
	"gitlab.com/oss-cloud/registry/pkg/common/context"
)

type Repository struct {
	Id   uuid.UUID `json:"id"`
	Path string    `json:"path"`

	DisplayName string `json:"displayName"`
}

type RepositoryInput struct {
	Path string `json:"path"`

	DisplayName string `json:"displayName"`
}

type RepositoryService interface {
	List(ctx context.Context) ([]Repository, error)
	Create(ctx context.Context, input RepositoryInput) (*Repository, error)
}
