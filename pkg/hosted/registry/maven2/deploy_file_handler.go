package maven2

import (
	"io"
	"net/http"

	"gitlab.com/oss-cloud/registry/pkg/hosted/registry/maven2/internal/repository"
	"gitlab.com/oss-cloud/registry/pkg/hosted/registry/maven2/internal/storage"

	"github.com/gorilla/mux"
	spec "github.com/opencontainers/image-spec/specs-go/v1"
	"go.uber.org/zap"

	"gitlab.com/oss-cloud/registry/pkg/common/logging"
	"gitlab.com/oss-cloud/registry/pkg/remote"
)

type deployFileHandler struct {
	Client *remote.Client
	logger *zap.Logger
}

func newDeployFileHandler(client *remote.Client) *deployFileHandler {
	return &deployFileHandler{
		Client: client,
		logger: logging.RootLogger.Named("registry/maven/deployFileHandler"),
	}
}

func (h *deployFileHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	path := r.Context().Value(mavenPathContextKey).(repository.MavenPath)
	registryName := storage.ToRegistryName(vars["repository"], path.Coordinate)
	manifest, err := h.Client.FetchManifest(r.Context(), registryName, path.Coordinate.Version)
	if err != nil {
		h.logger.Error(
			"failed to fetch manifest",
			zap.Error(err),
			zap.String("version", path.Coordinate.Version),
			zap.String("name", registryName),
		)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if manifest == nil {
		config, err := h.Client.CreateEmptyBlob(r.Context(), registryName)
		if err != nil {
			h.logger.Error(
				"failed to create placeholder config",
				zap.Error(err),
				zap.String("name", registryName),
			)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		manifest = &spec.Manifest{
			MediaType: spec.MediaTypeImageManifest,
			Config:    *config,
		}
	}

	if manifest.Layers == nil {
		manifest.Layers = make([]spec.Descriptor, 0, 1)
	}

	blob, err := h.Client.PushBlob(r.Context(), registryName, io.NopCloser(r.Body))
	if err != nil {
		h.logger.Error(
			"failed to pushing blob",
			zap.Error(err),
			zap.String("name", registryName),
		)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	blob.MediaType = "application/java-archive"
	if blob.Annotations == nil {
		blob.Annotations = make(map[string]string)
	}
	blob.Annotations["filename"] = path.Filename

	manifest.Layers = append(manifest.Layers, *blob)
	if err = h.Client.PushManifest(r.Context(), registryName, path.Coordinate.Version, manifest); err != nil {
		h.logger.Error(
			"failed to pushing manifest",
			zap.Error(err),
			zap.String("name", registryName),
			zap.String("version", path.Coordinate.Version),
		)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}
