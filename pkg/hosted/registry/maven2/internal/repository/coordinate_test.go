package repository

import (
	"fmt"
	"testing"
)

func TestParseCoordinate(t *testing.T) {
	for index, tc := range []struct {
		rawPath  string
		expected Coordinate
	}{
		{
			rawPath: "org/slf4j/slf4j-api/1.7.36/slf4j-api-1.7.36.pom",
			expected: Coordinate{
				GroupId:     "org.slf4j",
				ArtifactId:  "slf4j-api",
				Version:     "1.7.36",
				BaseVersion: "1.7.36",
				Extension:   "pom",
			},
		},
		{
			rawPath: "org/slf4j/slf4j-api/1.7.36/slf4j-api-1.7.36.pom.asc",
			expected: Coordinate{
				GroupId:       "org.slf4j",
				ArtifactId:    "slf4j-api",
				Version:       "1.7.36",
				BaseVersion:   "1.7.36",
				Extension:     "pom.asc",
				SignatureType: SignatureTypeAsc,
			},
		},
		{
			rawPath: "org/jruby/jruby/1.0RC1-SNAPSHOT/jruby-1.0RC1-20070504.160758-25-javadoc.jar",
			expected: Coordinate{
				GroupId:       "org.jruby",
				ArtifactId:    "jruby",
				Version:       "1.0RC1-20070504.160758-25",
				Timestamp:     1178294878000,
				BuildNumber:   25,
				BaseVersion:   "1.0RC1-SNAPSHOT",
				Classifier:    "javadoc",
				Extension:     "jar",
				SignatureType: "",
				Snapshot:      true,
			},
		},
		{
			rawPath: "com/sun/xml/ws/jaxws-local-transport/2.1.3/jaxws-local-transport-2.1.3.pom.md5",
			expected: Coordinate{
				GroupId:     "com.sun.xml.ws",
				ArtifactId:  "jaxws-local-transport",
				Version:     "2.1.3",
				BaseVersion: "2.1.3",
				Extension:   "pom.md5",
			},
		},
		{
			rawPath: "org/jruby/jruby/1.0RC1-SNAPSHOT/jruby-1.0RC1-20070504.160758-2.jar",
			expected: Coordinate{
				GroupId:     "org.jruby",
				ArtifactId:  "jruby",
				Version:     "1.0RC1-20070504.160758-2",
				BaseVersion: "1.0RC1-SNAPSHOT",
				Extension:   "jar",
				Snapshot:    true,
				Timestamp:   1178294878000,
				BuildNumber: 2,
			},
		},
		{
			rawPath:  "org/jruby",
			expected: Coordinate{},
		},
		{
			rawPath:  "maven-metadata.xml",
			expected: Coordinate{},
		},
		{
			rawPath:  "org/jruby/jruby",
			expected: Coordinate{},
		},
		{
			rawPath: "org/jruby/jruby/maven-metadata.xml",
			expected: Coordinate{
				GroupId:    "org.jruby",
				ArtifactId: "jruby",
			},
		},
	} {
		index, tc := index, tc
		t.Run(fmt.Sprintf("case-%d", index+1), func(t *testing.T) {
			t.Parallel()
			actual := parseCoordinate(tc.rawPath)
			if !tc.expected.Equals(actual) {
				t.Errorf("expected: %#v, got %#v", tc.expected, actual)
			}
		})
	}
}

func TestCoordinate_Equals(t *testing.T) {
	for index, testcase := range []struct {
		left, right Coordinate
		expected    bool
	}{
		{
			left:     Coordinate{},
			right:    Coordinate{},
			expected: true,
		},
		{
			left:     Coordinate{GroupId: "com.example"},
			right:    Coordinate{GroupId: "com.example"},
			expected: true,
		},
		{
			left:     Coordinate{GroupId: "com.example", ArtifactId: "foo"},
			right:    Coordinate{GroupId: "com.example"},
			expected: false,
		},
		{
			left:     Coordinate{GroupId: "com.example", ArtifactId: "foo"},
			right:    Coordinate{GroupId: "com.example", ArtifactId: "foo"},
			expected: true,
		},
		{
			left:     Coordinate{GroupId: "com.example", ArtifactId: "foo", Version: "1.0.0"},
			right:    Coordinate{GroupId: "com.example", ArtifactId: "foo", Version: "1.0.0"},
			expected: true,
		},
		{
			left:     Coordinate{GroupId: "com.example", ArtifactId: "foo", Version: "1.0.0"},
			right:    Coordinate{GroupId: "com.example", ArtifactId: "foo"},
			expected: false,
		},
		{
			left:     Coordinate{GroupId: "com.example", ArtifactId: "foo", Version: "1.0.0"},
			right:    Coordinate{GroupId: "com.example", ArtifactId: "foo", Version: "1.0.1"},
			expected: false,
		},
	} {
		index, testcase := index, testcase
		t.Run(fmt.Sprintf("case-%d", index+1), func(t *testing.T) {
			t.Parallel()
			actual := testcase.left.Equals(testcase.right)
			if actual != testcase.expected {
				t.Errorf("expected %t for case %d, got %t", testcase.expected, index+1, actual)
			}
		})
	}
}
