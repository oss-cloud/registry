//go:build wireinject
// +build wireinject

package remote

import (
	"net/http"

	"github.com/google/wire"
)

var WireSet = wire.NewSet(
	OptionsFromEnv,
	wire.Struct(new(Remote), "*"),
	NewClientWithOpts,
	NewRemoteClient,
)

func NewRemote() *Remote {
	wire.Build(
		OptionsFromEnv,
		NewRemoteWithOpts,
	)
	return nil
}

func NewRemoteWithOpts(options *Options) *Remote {
	wire.Build(
		wire.Struct(new(Remote), "*"),
		NewClientWithOpts,
	)
	return nil
}

func NewClient() *http.Client {
	wire.Build(NewClientWithOpts, OptionsFromEnv)
	return nil
}

func NewClientWithOpts(options *Options) *http.Client {
	wire.Build(
		newTlsConfig,
		newTransport,
		newClient,
	)
	return nil
}
