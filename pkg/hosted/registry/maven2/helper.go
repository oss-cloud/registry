package maven2

import (
	"net/http"
	"strings"

	"github.com/gorilla/mux"
)

func pathSuffix(needle string) mux.MatcherFunc {
	return func(r *http.Request, _ *mux.RouteMatch) bool {
		return strings.HasSuffix(r.URL.Path, needle)
	}
}
