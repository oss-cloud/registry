package proxyed

import (
	"context"

	"gitlab.com/oss-cloud/registry/pkg/hosted/registry/maven2/internal/repository"
	"gitlab.com/oss-cloud/registry/pkg/hosted/registry/maven2/internal/storage"

	spec "github.com/opencontainers/image-spec/specs-go/v1"
	"gitlab.com/oss-cloud/registry/pkg/common/upstream"

	"gitlab.com/oss-cloud/registry/pkg/remote"
)

type Manager struct {
	Remote   *remote.Remote
	Upstream *upstream.Client
}

func (m *Manager) ProvisionArtifact(ctx context.Context, upstream, repository string, path repository.MavenPath) {
	name := storage.ToRegistryName(repository, path.Coordinate)
	manifest, err := m.Remote.FetchManifest(ctx, name, path.Coordinate.Version)
	if err != nil {
		return
	}
	if path.Coordinate.Extension == "pom" {
		m.populateConfig(ctx, upstream, name, path, manifest)
		return
	}
}

func (m *Manager) populateConfig(ctx context.Context, upstream, name string, path repository.MavenPath, manifest *spec.Manifest) {
	if manifest != nil {
		m.Upstream.Fetch(ctx, upstream, name, manifest.Config.Annotations["etag"])
	}
}
