package mux

import (
	"github.com/gorilla/handlers"
	"go.uber.org/zap"
)

var _ handlers.RecoveryHandlerLogger = &recoverWriter{}

type recoverWriter struct {
	*zap.SugaredLogger
}

func (w recoverWriter) Println(args ...interface{}) {
	w.SugaredLogger.Info(args...)
}
