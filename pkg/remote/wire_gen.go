// Code generated by Wire. DO NOT EDIT.

//go:generate go run github.com/google/wire/cmd/wire
//go:build !wireinject
// +build !wireinject

package remote

import (
	"github.com/google/wire"
	"net/http"
)

// Injectors from wire.go:

func NewRemote() *Remote {
	options := OptionsFromEnv()
	remote := NewRemoteWithOpts(options)
	return remote
}

func NewRemoteWithOpts(options *Options) *Remote {
	client := NewClientWithOpts(options)
	remote := &Remote{
		Client:  client,
		Options: options,
	}
	return remote
}

func NewClient() *http.Client {
	options := OptionsFromEnv()
	client := NewClientWithOpts(options)
	return client
}

func NewClientWithOpts(options *Options) *http.Client {
	config := newTlsConfig(options)
	roundTripper := newTransport(config)
	client := newClient(roundTripper)
	return client
}

// wire.go:

var WireSet = wire.NewSet(
	OptionsFromEnv, wire.Struct(new(Remote), "*"), NewClientWithOpts,
	NewRemoteClient,
)
