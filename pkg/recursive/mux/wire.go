//go:build wireinject
// +build wireinject

package mux

import (
	"net/http"

	"gitlab.com/oss-cloud/registry/pkg/recursive/core/postgres"
	"gitlab.com/oss-cloud/registry/pkg/recursive/db"
	"gitlab.com/oss-cloud/registry/pkg/recursive/mux/rest"

	"github.com/google/wire"

	"gitlab.com/oss-cloud/registry/pkg/common/mux"
)

func NewHandler() (http.Handler, error) {
	wire.Build(
		mux.NewHandler,
		newRouter,
		rest.NewMount,
		db.ConnectDB,
		postgres.NewRepositoryService,
	)
	return nil, nil
}
