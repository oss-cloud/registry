package common

import (
	"net/http"

	"gitlab.com/oss-cloud/registry/pkg/hosted/registry"

	"github.com/gorilla/mux"
)

func CreateEnabledMiddleware(isEnabled func(*registry.Config) bool) mux.MiddlewareFunc {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			config := r.Context().Value(registry.ConfigContextKey).(*registry.Config)
			if !isEnabled(config) {
				w.Header().Set("content-type", "application/json")
				w.WriteHeader(http.StatusNotFound)
				w.Write([]byte(`{"error": {"code": "repository_disabled", "message": "npm repository is disabled"}}`))
				return
			}
			next.ServeHTTP(w, r)
		})
	}
}
