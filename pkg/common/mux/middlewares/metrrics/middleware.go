package metrrics

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"

	"github.com/prometheus/client_golang/prometheus"
)

var (
	requestCounter = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "http_requests_total",
	}, []string{"code", "method"})
	durationVec = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name: "http_request_duration",
	}, []string{"code", "method"})
	inflightGauge = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "http_request_inflight",
	})
	requestSizeVec = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name:    "http_request_size",
		Buckets: []float64{5, 10, 25, 50, 100, 250, 500, 1000, 2500, 5000, 10000},
	}, []string{"code", "method"})
	responseSizeVec = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name:    "http_response_size",
		Buckets: []float64{50, 100, 250, 500, 1000, 2500, 5000, 10000, 25000, 50000, 100000},
	}, []string{"code", "method"})
	timeToWriteVec = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name: "http_response_ttfb",
	}, []string{"code", "method"})
)

func init() {
	prometheus.MustRegister(
		requestCounter, durationVec, inflightGauge,
		responseSizeVec, requestSizeVec, timeToWriteVec,
	)
}

func Middleware(next http.Handler) (h http.Handler) {
	h = next
	h = promhttp.InstrumentHandlerCounter(requestCounter, h)
	h = promhttp.InstrumentHandlerDuration(durationVec, h)
	h = promhttp.InstrumentHandlerInFlight(inflightGauge, h)
	h = promhttp.InstrumentHandlerRequestSize(requestSizeVec, h)
	h = promhttp.InstrumentHandlerResponseSize(responseSizeVec, h)
	return
}
