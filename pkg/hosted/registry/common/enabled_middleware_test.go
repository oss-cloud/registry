package common

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/oss-cloud/registry/pkg/hosted/registry"
)

func TestCreateEnabledMiddleware(t *testing.T) {
	okHandler := http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		w.WriteHeader(http.StatusOK)
	})
	for index, tc := range []struct {
		isEnabled func(*registry.Config) bool
		expected  int
	}{
		{
			isEnabled: func(*registry.Config) bool { return false },
			expected:  http.StatusNotFound,
		},
		{
			isEnabled: func(*registry.Config) bool { return true },
			expected:  http.StatusOK,
		},
	} {
		index, tc := index, tc
		t.Run(fmt.Sprintf("case-%d", index+1), func(t *testing.T) {
			middleware := CreateEnabledMiddleware(tc.isEnabled)
			handler := middleware(okHandler)
			req := httptest.NewRequest(http.MethodGet, "/", nil).
				WithContext(context.WithValue(context.Background(), registry.ConfigContextKey, &registry.Config{}))
			resp := httptest.NewRecorder()
			handler.ServeHTTP(resp, req)
			if resp.Code != tc.expected {
				t.Errorf("expect return status %d for case %d, got %d", tc.expected, index+1, resp.Code)
			}
		})
	}
}
