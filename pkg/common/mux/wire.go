//go:build wireinject
// +build wireinject

package mux

import (
	"net/http"

	"github.com/google/wire"
	"github.com/gorilla/mux"
)

func NewHandler(m *mux.Router) http.Handler {
	wire.Build(
		newHandler,
	)
	return nil
}
