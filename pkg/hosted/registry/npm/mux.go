package npm

import (
	"net/http"

	"gitlab.com/oss-cloud/registry/pkg/hosted/registry"
	"gitlab.com/oss-cloud/registry/pkg/hosted/registry/common"

	"github.com/gorilla/mux"
)

type Mounter func(router *mux.Router)

func newMounter(
	packageFetchHandler *packageFetchHandler,
) Mounter {
	return func(router *mux.Router) {
		router.Use(common.CreateEnabledMiddleware(func(config *registry.Config) bool {
			return config.Registry.Npm.Enabled
		}))
		router.Path("/{package}").Methods(http.MethodGet).Handler(packageFetchHandler)
		router.Path("/{scope:@[a-z0-9_\\-\\.]+}/{package}").Methods(http.MethodGet).Handler(packageFetchHandler)
		router.Path("/-/ping").Methods(http.MethodGet).Handler(new(pingHandler))
	}
}

type pingHandler struct{}

func (h *pingHandler) ServeHTTP(w http.ResponseWriter, _ *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("{}"))
}
