package registry

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"go.uber.org/zap"

	"gitlab.com/oss-cloud/registry/pkg/common/context"
	"gitlab.com/oss-cloud/registry/pkg/common/errs"
	"gitlab.com/oss-cloud/registry/pkg/hosted/registry"
	"gitlab.com/oss-cloud/registry/pkg/remote"
)

type Middleware struct {
	Remote *remote.Remote
	Logger *zap.Logger
}

func NewMiddleware(
	remote *remote.Remote,
	logger *zap.Logger,
) *Middleware {
	return &Middleware{
		Remote: remote,
		Logger: logger.Named("mux.middleware.registry"),
	}
}

func (m *Middleware) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		repository := vars["repository"]

		config, err := m.fetchConfig(r.Context(), repository)
		if err != nil {
			w.Header().Set("Content-Type", "application/json")
			if unAuthorizedErr, ok := err.(*remote.UnAuthorizedError); ok {
				w.Header().Set("WWW-Authenticate", unAuthorizedErr.AuthenticateHeader())
				w.WriteHeader(http.StatusUnauthorized)
				w.Write([]byte(`{"error": {"code": "unauthorized", "message": "fail to authenticate backend"}}`))
			} else if err == errs.ErrForbidden {
				w.WriteHeader(http.StatusForbidden)
				w.Write([]byte(`{"error": {"code": "forbidden", "message": "backend forbidden"}}`))
			} else {
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(`{"error": {"code": "remote_failed", "message": "fail to connect backend"}}`))
			}
			return
		}

		if config == nil {
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte(`{"error": {"code": "repository_not_found", "message": "repository not found"}}`))
			return
		}

		ctx := context.WithValue(r.Context(), registry.ConfigContextKey, config)

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func (m *Middleware) fetchConfig(ctx context.Context, repository string) (*registry.Config, error) {
	blob, _, err := m.Remote.FetchConfig(ctx, repository, "latest")
	if err != nil {
		m.Logger.Error(
			"fail to fetch config",
			zap.Error(err),
			zap.String("repository", repository),
		)
		return nil, err
	}
	defer blob.Close()

	var config registry.Config
	if err = json.NewDecoder(blob).Decode(&config); err != nil {
		return nil, err
	}

	return &config, nil
}
