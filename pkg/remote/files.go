package remote

import spec "github.com/opencontainers/image-spec/specs-go/v1"

func FindFile(descriptors []spec.Descriptor, filename string) *spec.Descriptor {
	for _, descriptor := range descriptors {
		if descriptor.Annotations["filename"] == filename {
			return &descriptor
		}
	}
	return nil
}
