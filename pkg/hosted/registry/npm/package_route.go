package npm

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"time"

	"gitlab.com/oss-cloud/registry/pkg/hosted/registry"

	"github.com/gorilla/mux"
	spec "github.com/opencontainers/image-spec/specs-go/v1"
	"go.uber.org/zap"

	"gitlab.com/oss-cloud/registry/pkg/common/context"
	"gitlab.com/oss-cloud/registry/pkg/common/errs"
	"gitlab.com/oss-cloud/registry/pkg/common/ioutil"
	"gitlab.com/oss-cloud/registry/pkg/common/upstream"
	"gitlab.com/oss-cloud/registry/pkg/remote"
)

var _ http.Handler = &packageFetchHandler{}

const indexRefName = "index"
const mediaTypeNpmPackage = "application/vnd.registry.npm.package.v1+json"

type packageFetchHandler struct {
	remote   *remote.Client
	client   *http.Client
	logger   *zap.Logger
	upstream *upstream.Client
}

func newPackageFetchHandler(
	remote *remote.Client,
	client *http.Client,
	logger *zap.Logger,
	upstream *upstream.Client,
) *packageFetchHandler {
	return &packageFetchHandler{
		remote:   remote,
		client:   client,
		logger:   logger.Named("registry.npm.handler.package.fetch"),
		upstream: upstream,
	}
}

func (h *packageFetchHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	packageName := vars["package"]
	if scope, ok := vars["scope"]; ok {
		packageName = scope + "/" + packageName
	}

	registryName := fmt.Sprintf("%s/npm/%s", vars["repository"], packageName)

	if h.tryWithRegistry(r.Context(), registryName, packageName, w) {
		return
	}

	content, latestETag, err := h.fetchFromUpstream(r.Context(), packageName, "")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"error": "Internal Server Error"}`))
		return
	}

	buffer, err := ioutil.NewBufferFromReadCloser(content)
	if err != nil {
		h.logger.Error(
			"error when copying response",
			zap.String("packageName", packageName),
			zap.String("registryName", registryName),
			zap.Error(err),
		)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"error": "Internal Server Error"}`))
		return
	}

	go func(reader io.ReadSeekCloser) {
		if err = h.storeToRegistry(context.Background(), registryName, latestETag, reader); err != nil {
			h.logger.Error(
				"error when caching response",
				zap.String("packageName", packageName),
				zap.String("registryName", registryName),
				zap.String("etag", latestETag),
				zap.Error(err),
			)
		}
	}(buffer.Reader())

	if _, err = ioutil.CopyAndClose(w, buffer.Reader()); err != nil {
		h.logger.Error(
			"error when pipe response",
			zap.String("registryName", registryName),
			zap.Error(err),
		)
	}
}

func (h *packageFetchHandler) tryWithRegistry(ctx context.Context, registryName, packageName string, w http.ResponseWriter) (done bool) {
	config, manifest, err := h.remote.FetchConfig(ctx, registryName, indexRefName)
	if config != nil {
		defer config.Close()
	}

	header := w.Header()
	header.Set("Content-Type", "application/json")
	if err != nil {
		if unauthErr := new(remote.UnAuthorizedError); errors.As(err, unauthErr) || err == errs.ErrForbidden {
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte(`{"error": "Not Found"}`))
			return true
		} else {
			h.logger.Error(
				"backend error occur",
				zap.String("registryName", registryName),
				zap.Error(err),
			)
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(`{"error": "Internal Server Error"}`))
			return true
		}
	}

	if config == nil {
		return false
	}

	if etag, ok := manifest.Annotations["etag"]; manifest.Annotations != nil && ok {
		content, latestETag, err := h.fetchFromUpstream(ctx, packageName, etag)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(`{"error": "Internal Server Error"}`))
			return
		}
		if latestETag == "" {
			h.logger.Warn(
				"unable to re-validate cached package from upstream",
				zap.String("packageName", packageName),
			)
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte(`{"error": "Not Found"}`))
			return
		}

		if content != nil {
			buffer, err := ioutil.NewBufferFromReadCloser(content)
			if err != nil {
				h.logger.Error(
					"error when copying response",
					zap.String("packageName", packageName),
					zap.String("registryName", registryName),
					zap.Error(err),
				)
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(`{"error": "Internal Server Error"}`))
				return
			}

			go func(reader io.ReadSeekCloser) {
				if err = h.storeToRegistry(context.Background(), registryName, latestETag, reader); err != nil {
					h.logger.Error(
						"error when caching response",
						zap.String("packageName", packageName),
						zap.String("registryName", registryName),
						zap.String("etag", latestETag),
						zap.Error(err),
					)
				}
			}(buffer.Reader())

			w.Header().Set("ETag", latestETag)
			if _, err = io.Copy(w, buffer.Reader()); err != nil {
				h.logger.Error(
					"error when pipe response",
					zap.String("registryName", registryName),
					zap.Error(err),
				)
			}
			return true
		}
	}

	if _, err = io.Copy(w, config); err != nil {
		h.logger.Error(
			"error when pipe response",
			zap.String("registryName", registryName),
			zap.Error(err),
		)
	}
	return true
}

func (h *packageFetchHandler) fetchFromUpstream(ctx context.Context, packageName, etag string) (content io.ReadCloser, latestETag string, err error) {
	registryConfig := ctx.Value(registry.ConfigContextKey).(*registry.Config).Registry.Npm
	registryPrefix := registryConfig.Upstream.Registry

	return h.upstream.Fetch(ctx, registryPrefix, packageName, etag)
}

func (h *packageFetchHandler) storeToRegistry(ctx context.Context, registryName, etag string, buffer io.ReadSeekCloser) (err error) {
	defer buffer.Close()
	if _, ok := ctx.Deadline(); !ok {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, 10*time.Second)
		defer cancel()
	}

	return h.remote.StoreAsManifestConfig(
		&remote.StoreManifestConfigRequest{
			Context:      ctx,
			RegistryName: registryName,
			Ref:          indexRefName,
			Content:      buffer,
			PostDescriptor: func(descriptor *spec.Descriptor) {
				descriptor.MediaType = mediaTypeNpmPackage
				if etag != "" {
					descriptor.Annotations = map[string]string{
						"etag": etag,
					}
				}
			},
		},
	)
}
