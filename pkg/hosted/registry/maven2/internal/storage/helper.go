package storage

import (
	"fmt"
	"strings"

	"gitlab.com/oss-cloud/registry/pkg/hosted/registry/maven2/internal/repository"
)

func ToRegistryName(repository string, coordinate repository.Coordinate) string {
	return fmt.Sprintf("%s/maven/%s/%s", repository, strings.ReplaceAll(coordinate.GroupId, ".", "/"), coordinate.ArtifactId)
}
