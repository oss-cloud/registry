//go:build wireinject
// +build wireinject

package maven2

import (
	"github.com/google/wire"
	"gitlab.com/oss-cloud/registry/pkg/remote"
)

func NewMounter(
	*remote.Remote,
	*remote.Client,
) Mounter {
	wire.Build(
		newMounter,
		newFetchMetaHandler,
		newDeployFileHandler,
		newDeployFileHashHandler,
		newDeployPomFileHandler,
		newDeployMetaFileHandler,
		newFetchMetaFileHandler,
		newFetchFileHandler,
		newFetchFileHashHandler,
	)
	return nil
}
