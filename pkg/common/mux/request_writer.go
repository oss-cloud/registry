package mux

import (
	"io"

	"go.uber.org/zap"
)

var _ io.Writer = &requestWriter{}

type requestWriter struct {
	*zap.Logger
}

func (w *requestWriter) Write(p []byte) (n int, err error) {
	w.Logger.Info(string(p))
	return len(p), nil
}
