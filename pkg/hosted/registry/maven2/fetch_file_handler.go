package maven2

import (
	"net/http"

	"gitlab.com/oss-cloud/registry/pkg/hosted/registry/maven2/internal/repository"
	"gitlab.com/oss-cloud/registry/pkg/hosted/registry/maven2/internal/storage"

	"gitlab.com/oss-cloud/registry/pkg/common/logging"

	"github.com/gorilla/mux"
	spec "github.com/opencontainers/image-spec/specs-go/v1"
	"go.uber.org/zap"

	"gitlab.com/oss-cloud/registry/pkg/common/httphelper"
	"gitlab.com/oss-cloud/registry/pkg/common/ioutil"
	"gitlab.com/oss-cloud/registry/pkg/remote"
)

var _ http.Handler = &fetchFileHandler{}

type fetchFileHandler struct {
	Remote *remote.Remote
	logger *zap.Logger
}

func newFetchFileHandler(remote *remote.Remote) *fetchFileHandler {
	return &fetchFileHandler{
		Remote: remote,
		logger: logging.RootLogger.Named("registry/maven/fetchFileHashHandler"),
	}
}

func (h *fetchFileHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	path := r.Context().Value(mavenPathContextKey).(repository.MavenPath)
	registryName := storage.ToRegistryName(vars["repository"], path.Coordinate)
	version := path.Coordinate.Version
	if version == "" {
		version = "inedx"
	}

	logger := h.logger.With(
		zap.String("registryName", registryName),
		zap.String("ref", version),
	)

	manifest, err := h.Remote.FetchManifest(r.Context(), registryName, version)
	if err != nil {
		logger.Error("failed to fetch manifest", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if manifest == nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	var descriptor spec.Descriptor

	if version == "index" || path.Coordinate.Extension == "pom" {
		descriptor = manifest.Config
	} else {
		found := remote.FindFile(manifest.Layers, path.Filename)
		if found == nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		descriptor = *found
	}

	content, err := h.Remote.FetchBlob(r.Context(), registryName, descriptor.Digest)
	if err != nil {
		logger.Error("failed to fetch blob", zap.Error(err), zap.Stringer("digest", descriptor.Digest))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	header := w.Header()
	header.Set("Etag", httphelper.DigestToEtag(descriptor.Digest))
	header.Set("Content-Type", descriptor.MediaType)
	if _, err = ioutil.CopyAndClose(w, content); err != nil {
		h.logger.Error("failed to copy response", zap.Error(err))
	}
}
