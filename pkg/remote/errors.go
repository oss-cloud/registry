package remote

import (
	"fmt"
	"net/http"

	"github.com/containerd/containerd/remotes/docker/auth"

	"gitlab.com/oss-cloud/registry/pkg/common/errs"
)

type UnAuthorizedError struct {
	Type    string
	Realm   string
	Service string
	Scope   string
}

func (err UnAuthorizedError) Unwrap() error {
	return errs.ErrUnAuthorized
}

func (err UnAuthorizedError) Error() string {
	return fmt.Sprintf("registry: authenticate failed, www-authenticate: %q", err.AuthenticateHeader())
}

func (err UnAuthorizedError) AuthenticateHeader() string {
	message := fmt.Sprintf(`%s realm="%s",service="%s"`, err.Type, err.Realm, err.Service)
	if err.Scope != "" {
		message = fmt.Sprintf(`%s,scope="%s"`, message, err.Scope)
	}
	return message
}

func unAuthorizedErrorFromHeader(header http.Header) *UnAuthorizedError {
	challenge := auth.ParseAuthHeader(header)[0]
	var err UnAuthorizedError
	err.Type = schemeToString(challenge.Scheme)
	err.Realm = challenge.Parameters["realm"]
	err.Service = challenge.Parameters["service"]
	err.Scope = challenge.Parameters["scope"]
	return &err
}

func schemeToString(scheme auth.AuthenticationScheme) string {
	switch scheme {
	case auth.BasicAuth:
		return "Basic"
	case auth.BearerAuth:
		return "Bearer"
	}
	return ""

}
