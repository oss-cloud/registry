package pypi

import (
	_ "embed"
	"fmt"
	"html/template"
	"net/http"

	"gitlab.com/oss-cloud/registry/pkg/common/logging"

	"go.uber.org/zap"

	"github.com/gorilla/mux"

	"gitlab.com/oss-cloud/registry/pkg/remote"
)

//go:embed internal/template/version_list.gohtml
var packageVersionListTemplateStr string

var packageVersionListTemplate *template.Template

func init() {
	packageVersionListTemplate = template.Must(template.New("packageVersionList").Parse(packageVersionListTemplateStr))
}

var _ http.Handler = &packageVersionListHandle{}

type packageVersionListHandle struct {
	Remote *remote.Remote
	logger *zap.Logger
}

func newPackageVersionListHandler(
	remote *remote.Remote,
) *packageVersionListHandle {
	return &packageVersionListHandle{
		Remote: remote,
		logger: logging.RootLogger.Named("registry/pypi/packageVersionHandler"),
	}
}

func (h *packageVersionListHandle) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	registryName := fmt.Sprintf("%s/pypi/%s", vars["repository"], vars["package"])
	index, etag, err := h.Remote.FetchIndex(r.Context(), registryName, "index")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("internal server error"))
		return
	}

	if index == nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	header := w.Header()
	header.Set("Content-Type", "text/html; charset=UTF-8")
	if etag != "" {
		header.Set("Etag", etag)
	}

	if err = packageVersionListTemplate.Execute(w, index); err != nil {
		h.logger.With(
			zap.Error(err),
		).Error("failed to render")
	}
}
