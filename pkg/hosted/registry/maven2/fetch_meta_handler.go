package maven2

import (
	"net/http"

	"gitlab.com/oss-cloud/registry/pkg/hosted/registry/maven2/internal/repository"
	"gitlab.com/oss-cloud/registry/pkg/hosted/registry/maven2/internal/storage"

	"github.com/gorilla/mux"
	"go.uber.org/zap"

	"gitlab.com/oss-cloud/registry/pkg/common/ioutil"
	"gitlab.com/oss-cloud/registry/pkg/common/logging"
	"gitlab.com/oss-cloud/registry/pkg/remote"
)

type fetchMetaHandler struct {
	Remote *remote.Remote
	logger *zap.Logger
}

func newFetchMetaHandler(
	remote *remote.Remote,
) *fetchMetaHandler {
	return &fetchMetaHandler{
		Remote: remote,
		logger: logging.RootLogger.Named("registry/maven2/fetchMetaHandler"),
	}
}

func (h *fetchMetaHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	repositoryName := vars["repository"]
	path := r.Context().Value(mavenPathContextKey).(repository.MavenPath)
	registryName := storage.ToRegistryName(repositoryName, path.Coordinate)
	config, _, err := h.Remote.FetchConfig(r.Context(), registryName, path.Coordinate.Version)
	if err != nil {
		h.logger.With(
			zap.Error(err),
		).Error("error when fetching config")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("internal server error"))
		return
	}

	if config == nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "text/xml")

	if _, err = ioutil.CopyAndClose(w, config); err != nil {
		h.logger.With(
			zap.Error(err),
		).Error("error when copy to response")
		w.Write([]byte("internal server error"))
	}
}
