package repository

import (
	"strings"

	"gitlab.com/oss-cloud/registry/pkg/common/checksum"
)

type MavenPath struct {
	Path       string
	Filename   string
	HashType   checksum.HashType
	Coordinate Coordinate
}

func newPath(rawPath string, coordinate Coordinate) MavenPath {
	var ht checksum.HashType = ""
	for _, h := range checksum.HashTypeValues {
		if strings.HasSuffix(rawPath, "."+string(h)) {
			ht = h
			break
		}
	}

	return MavenPath{
		Path:       rawPath,
		Filename:   rawPath[strings.LastIndexByte(rawPath, '/')+1:],
		HashType:   ht,
		Coordinate: coordinate,
	}
}

func ParseMavenPath(path string) MavenPath {
	if path[0] == '/' {
		path = path[1:]
	}
	return newPath(path, parseCoordinate(path))
}
