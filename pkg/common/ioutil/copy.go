package ioutil

import "io"

func CopyAndClose(dst io.Writer, src io.ReadCloser) (written int64, err error) {
	defer src.Close()
	return io.Copy(dst, src)
}
