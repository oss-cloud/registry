package repository

import (
	"strconv"
	"strings"
	"time"

	"gitlab.com/oss-cloud/registry/pkg/common/checksum"
)

type Coordinate struct {
	GroupId       string
	ArtifactId    string
	Version       string
	BaseVersion   string
	Extension     string
	BuildNumber   int
	Timestamp     int64
	Snapshot      bool
	Classifier    string
	SignatureType SignatureType
}

func parseCoordinate(pathString string) Coordinate {
	str := pathString
	vEndPos := strings.LastIndexByte(str, '/')
	if vEndPos == -1 {
		return Coordinate{}
	}

	aEndPos := strings.LastIndexByte(str[:vEndPos-1], '/')
	if aEndPos == -1 {
		return Coordinate{}
	}

	gEndPos := strings.LastIndexByte(str[:aEndPos-1], '/')
	if gEndPos == -1 {
		return Coordinate{}
	}
	groupId := strings.ReplaceAll(str[0:gEndPos], "/", ".")
	artifactId := str[gEndPos+1 : aEndPos]
	baseVersion := str[aEndPos+1 : vEndPos]
	snapshot := strings.HasSuffix(baseVersion, SNAPSHOT_VERSION_SUFFIX)
	fileName := str[vEndPos+1:]
	str = fileName

	var extSuffix strings.Builder
	for _, hashType := range checksum.HashTypeValues {
		if strings.HasSuffix(str, "."+string(hashType)) {
			extSuffix.WriteByte('.')
			extSuffix.WriteString(string(hashType))
			str = str[:len(str)-len(string(hashType))-1]
			break
		}
	}

	signatureType := SignatureType("")
	for _, sType := range SignatureTypeValues {
		if strings.HasSuffix(str, "."+string(sType)) {
			var tmpBuilder strings.Builder
			tmpBuilder.WriteByte('.')
			tmpBuilder.WriteString(string(sType))
			tmpBuilder.WriteString(extSuffix.String())
			extSuffix = tmpBuilder
			signatureType = sType
			str = str[:len(str)-len(string(sType))-1]
			break
		}
	}

	if strings.HasSuffix(str, METADATA_FILENAME) {
		return Coordinate{
			GroupId:    groupId + "." + artifactId,
			ArtifactId: baseVersion,
		}
	}

	version := baseVersion
	var (
		tail        string
		timestamp   int64
		buildNumber int
	)
	if snapshot {
		vSnapshotStart := len(artifactId) + 1 + len(baseVersion) - len(SNAPSHOT_VERSION_SUFFIX)
		version = str[vSnapshotStart : vSnapshotStart+len(SNAPSHOT_VERSION_SUFFIX)]
		if version == SNAPSHOT_VERSION_SUFFIX {
			vTimestampStart := vSnapshotStart + len(version) + 1
			version = baseVersion
			tail = str[len(artifactId)+len(baseVersion)+1:]

			if len(str) > vTimestampStart+len(DOTTED_TIMESTAMP_VERSION_FORMAT) {
				if _, err := time.Parse(DOTTED_TIMESTAMP_VERSION_FORMAT, str[vTimestampStart:vTimestampStart+len(DOTTED_TIMESTAMP_VERSION_FORMAT)]); err == nil {
					version = str[vTimestampStart : vTimestampStart+len(SNAPSHOT_VERSION_SUFFIX)]
					vSnapshotStart = vTimestampStart
					tail = ""
				}
			}
		}

		if tail == "" {
			var snapshotTimestampedVersion strings.Builder
			snapshotTimestampedVersion.WriteString(version)
			snapshotTimestampedVersion.WriteString(str[vSnapshotStart+len(version) : vSnapshotStart+len(version)+len(SNAPSHOT_VERSION_SUFFIX)-1])
			if t, err := time.Parse(DOTTED_TIMESTAMP_VERSION_FORMAT, snapshotTimestampedVersion.String()); err == nil {
				timestamp = t.UnixMilli()
			}

			snapshotTimestampedVersion.WriteByte('-')
			buildNumberPos := vSnapshotStart + snapshotTimestampedVersion.Len()
			var bnr strings.Builder
			for str[buildNumberPos] >= '0' && str[buildNumberPos] <= '9' {
				snapshotTimestampedVersion.WriteByte(str[buildNumberPos])
				bnr.WriteByte(str[buildNumberPos])
				buildNumberPos += 1
			}
			if tmpBuildNumber, err := strconv.Atoi(bnr.String()); err == nil {
				buildNumber = tmpBuildNumber
			}
			tail = str[vSnapshotStart+snapshotTimestampedVersion.Len():]
			version = baseVersion[:len(baseVersion)-len(SNAPSHOT_VERSION_SUFFIX)] + snapshotTimestampedVersion.String()
		}
	} else {
		filenameStr, artifactStr := strings.ToLower(fileName), strings.ToLower(artifactId+"-"+baseVersion)

		if !strings.HasPrefix(filenameStr, artifactStr) || (filenameStr[len(artifactStr)] != '-' && filenameStr[len(artifactStr)] != '.') {
			return Coordinate{}
		}

		nTailPos := len(artifactId) + len(baseVersion) + 1
		tail = str[nTailPos:]
	}

	nExtPos := getExceptionPos(tail)
	if nExtPos == -1 {
		return Coordinate{}
	}

	ext := tail[nExtPos+1:]
	classifier := ""
	if tail[0] == '-' {
		classifier = tail[1:nExtPos]
	}

	return Coordinate{
		Snapshot:      snapshot,
		GroupId:       groupId,
		ArtifactId:    artifactId,
		Version:       version,
		Timestamp:     timestamp,
		BuildNumber:   buildNumber,
		BaseVersion:   baseVersion,
		Classifier:    classifier,
		Extension:     ext + extSuffix.String(),
		SignatureType: signatureType,
	}
}

func getExceptionPos(tail string) int {
	nExtPos := strings.LastIndexByte(tail, '.')
	if nExtPos == -1 {
		return -1
	}

	tailWithoutExt := tail[:nExtPos]

	if strings.HasSuffix(tailWithoutExt, TAR_EXT_PREFIX) {
		nExtPos -= len(TAR_EXT_PREFIX)
	} else if strings.HasSuffix(tailWithoutExt, CPIO_EXT_PREFIX) {
		nExtPos -= len(CPIO_EXT_PREFIX)
	}

	return nExtPos
}

func (c Coordinate) Equals(other Coordinate) bool {
	return c.GroupId == other.GroupId &&
		c.ArtifactId == other.ArtifactId &&
		c.Version == other.Version &&
		c.BaseVersion == other.BaseVersion &&
		c.Extension == other.Extension &&
		c.BuildNumber == other.BuildNumber &&
		c.Timestamp == other.Timestamp &&
		c.Snapshot == other.Snapshot &&
		c.Classifier == other.Classifier &&
		c.SignatureType == other.SignatureType
}
