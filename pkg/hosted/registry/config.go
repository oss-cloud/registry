package registry

import "gitlab.com/oss-cloud/registry/pkg/common/context"

var (
	ConfigContextKey        = context.Key("registry/config")
	AuthorizationContextKey = context.Key("registry/authorization")
)

type Config struct {
	Registry struct {
		Npm   *NpmConfig   `json:"npm,omitempty"`
		Maven *MavenConfig `json:"maven,omitempty"`
		Pypi  *PypiConfig  `json:"pypi,omitempty"`
	} `json:"registries"`
}

type PypiConfig struct {
	Enabled  bool `json:"enabled,omitempty"`
	Upstream struct {
		Registry string `json:"registry"`
		Cache    struct {
			Enabled bool `json:"enabled"`
		} `json:"cache"`
	} `json:"upstream,omitempty"`
}

type NpmConfig struct {
	Enabled  bool `json:"enabled,omitempty"`
	Upstream struct {
		Registry string `json:"registry"`
		Cache    struct {
			Enabled bool `json:"enabled"`
		} `json:"cache"`
	} `json:"upstream,omitempty"`
}

type MavenConfig struct {
	Enabled bool `json:"enabled"`
}
