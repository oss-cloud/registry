package postgres

import (
	"database/sql"

	"github.com/google/uuid"

	"go.uber.org/zap"

	"gitlab.com/oss-cloud/registry/pkg/common/context"
	"gitlab.com/oss-cloud/registry/pkg/common/logging"
	"gitlab.com/oss-cloud/registry/pkg/recursive/core"
)

var _ core.RepositoryService = &repositoryService{}

type repositoryService struct {
	logger *zap.Logger
	DB     *sql.DB
}

func newRepositoryService(db *sql.DB) *repositoryService {
	return &repositoryService{
		logger: logging.RootLogger.Named("core/postgres/repositoryService"),
		DB:     db,
	}
}

func (s *repositoryService) List(ctx context.Context) ([]core.Repository, error) {
	logger := s.logger.With(zap.String("method", "list"))

	rows, err := s.DB.QueryContext(ctx, "SELECT id, url_path, display_name FROM repository ORDER BY url_path")
	if err != nil {
		logger.Error("failed to query", zap.Error(err))
		return nil, err
	}

	defer rows.Close()

	result := make([]core.Repository, 0)
	for rows.Next() {
		var repo core.Repository
		if err = rows.Scan(&repo.Id, &repo.Path, &repo.DisplayName); err != nil {
			return nil, err
		}
		result = append(result, repo)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return result, nil
}

func (s *repositoryService) Create(ctx context.Context, input core.RepositoryInput) (*core.Repository, error) {
	tx, err := s.DB.Begin()
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	id, _ := uuid.NewUUID()

	_, err = tx.ExecContext(
		ctx,
		"INSERT INTO repository (id, url_path, display_name) VALUES ($1, $2, $3) RETURNING id, url_path, display_name",
		id, input.Path, input.DisplayName,
	)

	if err != nil {
		return nil, err
	}

	if err = tx.Commit(); err != nil {
		return nil, err
	}

	return &core.Repository{
		Id:          id,
		Path:        input.Path,
		DisplayName: input.DisplayName,
	}, nil
}
