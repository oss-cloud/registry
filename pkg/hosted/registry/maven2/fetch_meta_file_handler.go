package maven2

import (
	"net/http"

	"gitlab.com/oss-cloud/registry/pkg/hosted/registry/maven2/internal/repository"
	"gitlab.com/oss-cloud/registry/pkg/hosted/registry/maven2/internal/storage"

	"gitlab.com/oss-cloud/registry/pkg/common/logging"

	"github.com/gorilla/mux"
	"go.uber.org/zap"

	"gitlab.com/oss-cloud/registry/pkg/common/httphelper"
	"gitlab.com/oss-cloud/registry/pkg/common/ioutil"
	"gitlab.com/oss-cloud/registry/pkg/remote"
)

var _ http.Handler = &fetchMetaFileHandler{}

type fetchMetaFileHandler struct {
	Remote *remote.Remote
	logger *zap.Logger
}

func newFetchMetaFileHandler(remote *remote.Remote) *fetchMetaFileHandler {
	return &fetchMetaFileHandler{
		Remote: remote,
		logger: logging.RootLogger.Named("registry/maven/fetchMetaFileHandler"),
	}
}

func (h *fetchMetaFileHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	path := r.Context().Value(mavenPathContextKey).(repository.MavenPath)
	registryName := storage.ToRegistryName(vars["repository"], path.Coordinate)

	logger := h.logger.With(
		zap.String("registryName", registryName),
	)

	const ref = "index"

	manifest, err := h.Remote.FetchManifest(r.Context(), registryName, ref)
	if err != nil {
		logger.Error("failed to fetch manifest", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if manifest == nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	content, err := h.Remote.FetchBlob(r.Context(), registryName, manifest.Layers[0].Digest)

	if content == nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	w.Header().Set("Etag", httphelper.DigestToEtag(manifest.Config.Digest))
	if _, err := ioutil.CopyAndClose(w, content); err != nil {
		logger.Error("failed to pipe response", zap.Error(err))
	}
}
