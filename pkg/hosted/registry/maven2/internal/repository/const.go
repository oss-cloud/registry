package repository

const (
	// File name of Maven2 repository metadata files.
	METADATA_FILENAME = "maven-metadata.xml"

	// File name of Maven2 archetype catalog file.
	ARCHETYPE_CATALOG_FILENAME = "archetype-catalog.xml"

	// Layout to be used for a DateTimeFormatter, which maven typically uses in snapshot versions.
	DOTTED_TIMESTAMP_VERSION_FORMAT = "20060102.150405"

	// Content Type of Maven2 checksum files (sha1, md5).
	CHECKSUM_CONTENT_TYPE = "text/plain"

	// The suffix of base version for snapshots.
	SNAPSHOT_VERSION_SUFFIX = "SNAPSHOT"

	// The base path and name of maven index file assets.
	INDEX_FILE_BASE_PATH = ".index/nexus-maven-repository-index"

	// The full path of maven index property file assets.
	INDEX_PROPERTY_FILE_PATH = INDEX_FILE_BASE_PATH + ".properties"

	// The full path of maven index chunk file assets.
	INDEX_MAIN_CHUNK_FILE_PATH = INDEX_FILE_BASE_PATH + ".gz"

	TAR_EXT_PREFIX = ".tar"

	CPIO_EXT_PREFIX = ".cpio"
)
