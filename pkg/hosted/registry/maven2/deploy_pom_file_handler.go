package maven2

import (
	"io"
	"net/http"

	"gitlab.com/oss-cloud/registry/pkg/hosted/registry/maven2/internal/repository"
	"gitlab.com/oss-cloud/registry/pkg/hosted/registry/maven2/internal/storage"

	"github.com/gorilla/mux"
	spec "github.com/opencontainers/image-spec/specs-go/v1"
	"go.uber.org/zap"

	"gitlab.com/oss-cloud/registry/pkg/common/logging"
	"gitlab.com/oss-cloud/registry/pkg/remote"
)

var _ http.Handler = &deployPomFileHandler{}

type deployPomFileHandler struct {
	Client *remote.Client
	logger *zap.Logger
}

func newDeployPomFileHandler(client *remote.Client) *deployPomFileHandler {
	return &deployPomFileHandler{
		Client: client,
		logger: logging.RootLogger.Named("registry/maven/deployPomFileHandler"),
	}
}

func (h *deployPomFileHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	path := r.Context().Value(mavenPathContextKey).(repository.MavenPath)
	registryName := storage.ToRegistryName(vars["repository"], path.Coordinate)

	logger := h.logger.With(
		zap.String("registryName", registryName),
		zap.String("version", path.Coordinate.Version),
	)

	manifest, err := h.Client.FetchManifest(r.Context(), registryName, path.Coordinate.Version)
	if err != nil {
		logger.Error("failed to fetch manifest", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if manifest == nil {
		manifest = &spec.Manifest{
			MediaType: spec.MediaTypeImageManifest,
		}
	}

	blob, err := h.Client.PushBlob(r.Context(), registryName, io.NopCloser(r.Body))
	if err != nil {
		logger.Error(
			"failed to pushing blob",
			zap.Error(err),
			zap.String("name", registryName),
		)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	blob.MediaType = "application/vnd.oss-cloud.registry.maven.pom+xml"
	blob.Annotations["filename"] = path.Filename

	config, err := h.Client.CreateEmptyBlob(r.Context(), registryName)
	if err != nil {
		h.logger.Error(
			"failed to create placeholder config",
			zap.Error(err),
			zap.String("name", registryName),
		)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	config.MediaType = "application/vnd.oss-cloud.registry.maven+json"

	manifest.Config = *config

	if manifest.Layers == nil {
		manifest.Layers = make([]spec.Descriptor, 0, 1)
	}

	manifest.Layers = append(manifest.Layers, *blob)

	if err = h.Client.PushManifest(r.Context(), registryName, path.Coordinate.Version, manifest); err != nil {
		logger.Error(
			"failed to pushing manifest",
			zap.Error(err),
		)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}
