package remote

import (
	"bytes"
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"path"

	"github.com/opencontainers/go-digest"
	spec "github.com/opencontainers/image-spec/specs-go/v1"

	"gitlab.com/oss-cloud/registry/pkg/common/context"
	"gitlab.com/oss-cloud/registry/pkg/common/errs"
	"gitlab.com/oss-cloud/registry/pkg/common/ioutil"
	"gitlab.com/oss-cloud/registry/pkg/common/pooled"
	"gitlab.com/oss-cloud/registry/pkg/hosted/registry"
)

type Remote struct {
	Client  *http.Client
	Options *Options
}

func (r *Remote) PushManifest(ctx context.Context, name, ref string, manifest *spec.Manifest) (err error) {
	if manifest.SchemaVersion == 0 {
		manifest.SchemaVersion = 2
	}
	manifest.MediaType = spec.MediaTypeImageManifest
	buffer := pooled.Buffer.Get().(*bytes.Buffer)
	defer pooled.Buffer.Put(buffer)
	buffer.Reset()

	body, err := json.Marshal(manifest)
	if err != nil {
		return err
	}

	uri := &url.URL{
		Scheme: r.scheme(),
		Host:   r.Options.Host,
		Path:   path.Join("/v2", r.Options.Prefix, name, "manifests", ref),
	}
	req, err := http.NewRequest(http.MethodPut, uri.String(), bytes.NewReader(body))
	if err != nil {
		return err
	}
	req.Header.Set("content-type", spec.MediaTypeImageManifest)

	resp, err := r.do(ctx, req)
	if err != nil {
		return
	}

	if resp == nil {
		return errs.ErrNotFound
	}

	if resp != nil && resp.Body != nil {
		defer resp.Body.Close()
	}

	switch resp.StatusCode {
	case http.StatusCreated:
		return nil
	case http.StatusUnauthorized:
		return unAuthorizedErrorFromHeader(resp.Header)
	case http.StatusBadRequest:
		content, _ := httputil.DumpResponse(resp, true)
		log.Println(string(content))
		return errs.ErrUnexpectedStatus
	default:
		return errs.ErrUnexpectedStatus
	}
}

func (r *Remote) PushBlob(ctx context.Context, name string, content io.ReadCloser) (_ *spec.Descriptor, err error) {
	location, err := r.createUploadSession(ctx, name)
	if err != nil {
		return
	}
	reader, err := ioutil.FromReader(content)
	if err != nil {
		return nil, err
	}
	defer reader.Close()
	return r.performUpload(ctx, location, reader)
}

func (r *Remote) performUpload(ctx context.Context, location string, content ioutil.Reader) (_ *spec.Descriptor, err error) {
	hash := sha256.New()
	if _, err = io.Copy(hash, content); err != nil {
		return
	}
	dig := digest.NewDigest(digest.SHA256, hash)

	if _, err = content.Seek(0, io.SeekStart); err != nil {
		return
	}

	req, err := http.NewRequest(http.MethodPut, location, content)
	if err != nil {
		return
	}

	req.Header.Set("Content-Type", "application/octet-stream")

	query := req.URL.Query()
	query.Set("digest", dig.String())
	req.URL.RawQuery = query.Encode()

	resp, err := r.do(ctx, req)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusCreated {
		return nil, errs.ErrUnexpectedStatus
	}

	return &spec.Descriptor{
		Digest: dig,
		Size:   int64(content.Len()),
	}, nil
}

func (r *Remote) createUploadSession(ctx context.Context, name string) (location string, err error) {
	uri := &url.URL{
		Scheme: r.scheme(),
		Host:   r.Options.Host,
		Path:   path.Join("/v2", r.Options.Prefix, name, "/blobs/uploads") + "/",
	}

	req := &http.Request{
		Method: http.MethodPost,
		URL:    uri,
		Header: make(http.Header),
	}

	resp, err := r.do(ctx, req)
	if err != nil {
		return "", err
	}

	if resp.StatusCode == http.StatusAccepted {
		return resp.Header.Get("Location"), nil
	}

	return "", errs.ErrUnexpectedStatus
}

func (r *Remote) FetchConfig(ctx context.Context, name, ref string) (io.ReadCloser, *spec.Manifest, error) {
	manifest, err := r.FetchManifest(ctx, name, ref)
	if err != nil {
		return nil, nil, err
	}

	if manifest == nil {
		return nil, nil, nil
	}

	content, err := r.FetchBlob(ctx, name, manifest.Config.Digest)
	if err != nil {
		return nil, nil, err
	}
	return content, manifest, nil
}

func (r *Remote) FetchIndex(ctx context.Context, name, ref string) (*spec.Index, string, error) {
	var index spec.Index
	etag, err := r.fetchManifest(ctx, name, ref, spec.MediaTypeImageIndex, &index)
	if err != nil {
		return nil, "", err
	}
	return &index, etag, nil
}

func (r *Remote) FetchManifest(ctx context.Context, name, ref string) (*spec.Manifest, error) {
	var manifest spec.Manifest
	if _, err := r.fetchManifest(ctx, name, ref, spec.MediaTypeImageManifest, &manifest); err != nil {
		return nil, err
	}

	if manifest.MediaType == "" {
		return nil, nil
	}
	return &manifest, nil
}

func (r *Remote) fetchManifest(ctx context.Context, name, ref, contentType string, output interface{}) (etag string, err error) {
	uri := &url.URL{
		Scheme: r.scheme(),
		Host:   r.Options.Host,
		Path:   path.Join("/v2", r.Options.Prefix, name, "manifests", ref),
	}

	req := &http.Request{
		URL:    uri,
		Method: http.MethodGet,
		Header: make(http.Header),
	}

	req.Header.Set("accept", contentType)

	resp, err := r.do(ctx, req.WithContext(ctx))
	if err != nil {
		return "", err
	} else if resp == nil {
		return "", nil
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return "", errs.ErrUnexpectedStatus
	}

	if err = json.NewDecoder(resp.Body).Decode(&output); err != nil {
		return "", err
	}

	return resp.Header.Get("Etag"), nil
}

func (r *Remote) FetchBlob(ctx context.Context, name string, digest digest.Digest) (io.ReadCloser, error) {
	uri := &url.URL{
		Scheme: r.scheme(),
		Host:   r.Options.Host,
		Path:   path.Join("/v2", r.Options.Prefix, name, "blobs", string(digest)),
	}

	req := &http.Request{
		URL:    uri,
		Method: http.MethodGet,
		Header: make(http.Header),
	}

	resp, err := r.do(ctx, req)
	if err != nil {
		return nil, err
	} else if resp == nil {
		return nil, nil
	}

	switch resp.StatusCode {
	case http.StatusOK:
		return resp.Body, nil
	default:
		resp.Body.Close()
		return nil, errs.ErrUnexpectedStatus
	}
}

func (r *Remote) do(ctx context.Context, req *http.Request) (*http.Response, error) {
	authorization, ok := ctx.Value(registry.AuthorizationContextKey).(string)
	if ok {
		req.Header.Set("authorization", authorization)
	}

	resp, err := r.Client.Do(req.WithContext(ctx))
	if err != nil {
		return nil, err
	}

	if resp.StatusCode == http.StatusUnauthorized {
		resp.Body.Close()
		authorization, err = r.attemptAnonymousLogin(ctx, unAuthorizedErrorFromHeader(resp.Header))
		if err != nil {
			return nil, err
		}
		req.Header.Set("Authorization", authorization)
		resp, err = r.Client.Do(req.WithContext(ctx))
		if err != nil {
			return nil, err
		}
		context.WithValue(ctx, registry.AuthorizationContextKey, authorization)
	}

	switch resp.StatusCode {
	case http.StatusForbidden:
		resp.Body.Close()
		return nil, errs.ErrForbidden
	case http.StatusUnauthorized:
		resp.Body.Close()
		return nil, unAuthorizedErrorFromHeader(resp.Header)
	case http.StatusNotFound:
		resp.Body.Close()
		return nil, nil
	default:
		return resp, nil
	}
}

func (r *Remote) attemptAnonymousLogin(ctx context.Context, unAuthorizedError *UnAuthorizedError) (string, error) {
	query := make(url.Values)
	query.Set("service", unAuthorizedError.Service)
	if unAuthorizedError.Scope != "" {
		query.Set("scope", unAuthorizedError.Scope)
	}

	req, err := http.NewRequest(http.MethodGet, unAuthorizedError.Realm+"?"+query.Encode(), nil)
	if err != nil {
		return "", err
	}

	authorization, ok := ctx.Value(registry.AuthorizationContextKey).(string)
	if ok {
		req.Header.Set("authorization", authorization)
	}

	resp, err := r.Client.Do(req.WithContext(ctx))
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	var tokenResponse struct {
		Token string `json:"token"`
	}

	if err = json.NewDecoder(resp.Body).Decode(&tokenResponse); err != nil {
		return "", err
	}

	return fmt.Sprintf("%s %s", unAuthorizedError.Type, tokenResponse.Token), nil
}

func (r *Remote) scheme() string {
	scheme := "https"
	if r.Options.PlainHTTP {
		scheme = "http"
	}
	return scheme
}
