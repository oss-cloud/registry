package context

import (
	ctx "context"
)

type Context = ctx.Context
type CancelFunc = ctx.CancelFunc

type Key string

var (
	Background   = ctx.Background
	WithDeadline = ctx.WithDeadline
	WithTimeout  = ctx.WithTimeout
)

func WithValue(parent Context, key Key, value any) Context {
	if vctx, ok := parent.(*ValueContext); ok {
		vctx.Values[key] = value
		return vctx
	}
	return &ValueContext{
		Context: parent,
		Values: map[Key]any{
			key: value,
		},
	}
}

type ValueContext struct {
	Context
	Values map[Key]any
}

func (c *ValueContext) Value(key any) any {
	ctxKey, ok := key.(Key)
	if !ok {
		return c.Context.Value(key)
	}
	if value, ok := c.Values[ctxKey]; ok {
		return value
	}
	return c.Context.Value(key)
}

func (c *ValueContext) Set(key Key, value any) {
	c.Values[key] = value
}
