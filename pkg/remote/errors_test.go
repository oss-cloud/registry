package remote

import (
	"net/http"
	"testing"
)

func TestUnAuthorizedErrorFromHeader(t *testing.T) {
	headers := make(http.Header)
	headers.Set("WWW-Authenticate", `Bearer realm="https://auth.docker.io/token",service="registry.docker.io",scope="repository:library/nginx:pull"`)
	err := unAuthorizedErrorFromHeader(headers)
	if err.Type != "Bearer" {
		t.Errorf("expect type `Bearer`, got: %q", err.Type)
	}

	if err.Realm != "https://auth.docker.io/token" {
		t.Errorf("expect realm https://auth.docker.io/token, got: %q", err.Realm)
	}

	if err.Service != "registry.docker.io" {
		t.Errorf("expect service registry.docker.io, got: %q", err.Service)
	}

	if err.Scope != "repository:library/nginx:pull" {
		t.Errorf("expect realm repository:library/nginx:pull, got: %q", err.Scope)
	}
}
func TestUnAuthorizedErrorFromHeaderOfHarbor(t *testing.T) {
	headers := make(http.Header)
	headers.Set("WWW-Authenticate", `Bearer realm="https://harbor.local/service/token",service="harbor-registry",scope="repository:registry/hosted:pull,push"`)
	err := unAuthorizedErrorFromHeader(headers)
	if err.Type != "Bearer" {
		t.Errorf("expect type `Bearer`, got: %q", err.Type)
	}

	if err.Realm != "https://harbor.local/service/token" {
		t.Errorf("expect realm https://harbor.local/service/token, got: %q", err.Realm)
	}

	if err.Service != "harbor-registry" {
		t.Errorf("expect service harbor-registry, got: %q", err.Service)
	}

	if err.Scope != "repository:registry/hosted:pull,push" {
		t.Errorf("expect realm repository:registry/hosted:pull,push, got: %q", err.Scope)
	}
}
