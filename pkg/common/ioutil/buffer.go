package ioutil

import (
	"bytes"
	"io"
	"sync"

	"gitlab.com/oss-cloud/registry/pkg/common/pooled"
)

type Buffer interface {
	Reader() Reader
}

type Reader interface {
	io.ReadSeekCloser
	Len() int
}

func FromReader(r io.ReadCloser) (reader Reader, err error) {
	if reader, ok := r.(Reader); ok {
		return reader, nil
	}

	defer r.Close()
	content, err := io.ReadAll(r)
	if err != nil {
		return nil, err
	}
	return &lenReader{
		Reader: bytes.NewReader(content),
		length: len(content),
	}, nil
}

type lenReader struct {
	*bytes.Reader
	length int
}

func (r *lenReader) Len() int {
	return r.length
}

func (r *lenReader) Close() error { return nil }

type controlledReader struct {
	*lenReader
	buffer *buffer
}

func (r *controlledReader) Read(p []byte) (n int, err error) {
	if r.buffer != nil && r.buffer.disabled {
		return 0, io.ErrClosedPipe
	}
	return r.Reader.Read(p)
}

func (r *controlledReader) Close() error {
	r.buffer.release()
	return nil
}

type buffer struct {
	*bytes.Buffer
	disabled   bool
	m          sync.Mutex
	usageCount uint32
}

func (b *buffer) Reader() Reader {
	if b.disabled {
		return nil
	}
	b.m.Lock()
	defer b.m.Unlock()
	b.usageCount += 1
	return &controlledReader{
		lenReader: &lenReader{
			Reader: bytes.NewReader(b.Buffer.Bytes()),
			length: b.Len(),
		},
		buffer: b,
	}
}

func (b *buffer) release() {
	b.m.Lock()
	defer b.m.Unlock()
	b.usageCount -= 1
	if b.usageCount == 0 {
		pooled.Buffer.Put(b.Buffer)
	}
}

func NewBufferFromReadCloser(reader io.ReadCloser) (Buffer, error) {
	defer reader.Close()
	b := &buffer{
		Buffer: pooled.Buffer.Get().(*bytes.Buffer),
	}
	b.Buffer.Reset()
	if _, err := io.Copy(b.Buffer, reader); err != nil {
		return nil, err
	}

	return b, nil
}
