package pypi

import (
	"net/http"

	"gitlab.com/oss-cloud/registry/pkg/hosted/registry"
	"gitlab.com/oss-cloud/registry/pkg/hosted/registry/common"

	"github.com/gorilla/mux"
)

type Mounter func(*mux.Router)

func newMounter(
	packageVersionListHandle *packageVersionListHandle,
	packageDownloadHandler *packageDownloadHandler,
) Mounter {
	return func(router *mux.Router) {
		router.Use(common.CreateEnabledMiddleware(func(config *registry.Config) bool { return config.Registry.Pypi.Enabled }))
		router.Methods(http.MethodGet).Path("/{package}/").Handler(packageVersionListHandle)
		router.Methods(http.MethodGet).Path("/{package}/{digest}/{filename}").Handler(packageDownloadHandler)
	}
}
