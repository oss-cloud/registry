package remote

import (
	"crypto/tls"
	"errors"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	requestCounter = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "client_http_request_total",
	}, []string{"code", "method"})
	requestDurationObserve = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name: "client_http_request_duration",
	}, []string{"code", "method"})
	inflightGauge = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "client_http_request_inflight",
	})
)

func init() {
	prometheus.MustRegister(requestCounter, requestDurationObserve, inflightGauge)
}

func newTlsConfig(options *Options) (cfg *tls.Config) {
	cfg = &tls.Config{}
	if options.Insecure {
		cfg.InsecureSkipVerify = true
	}
	return cfg
}

func newTransport(tlsConfig *tls.Config) (t http.RoundTripper) {
	t = &http.Transport{
		TLSClientConfig: tlsConfig,
	}
	t = promhttp.InstrumentRoundTripperCounter(requestCounter, t)
	t = promhttp.InstrumentRoundTripperDuration(requestDurationObserve, t)
	t = promhttp.InstrumentRoundTripperInFlight(inflightGauge, t)
	return
}

func newClient(t http.RoundTripper) *http.Client {
	return &http.Client{
		Transport: t,
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			if req.Method != http.MethodGet {
				return http.ErrUseLastResponse
			}
			if len(via) >= 10 {
				return errors.New("stopped after 10 redirects")
			}
			return nil
		},
	}
}
