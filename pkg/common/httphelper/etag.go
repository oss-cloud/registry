package httphelper

import (
	"strings"

	"github.com/opencontainers/go-digest"
)

func DigestToEtag(dig digest.Digest) string {
	var builder strings.Builder
	builder.WriteByte('"')
	builder.WriteString(dig.String())
	builder.WriteByte('"')
	return builder.String()
}
