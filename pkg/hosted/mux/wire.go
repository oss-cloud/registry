//go:build wireinject
// +build wireinject

package mux

import (
	"net/http"

	"gitlab.com/oss-cloud/registry/pkg/common/mux"
	"gitlab.com/oss-cloud/registry/pkg/hosted/middlewares/registry"
	"gitlab.com/oss-cloud/registry/pkg/hosted/mux/rest"
	"gitlab.com/oss-cloud/registry/pkg/hosted/registry/maven2"
	"gitlab.com/oss-cloud/registry/pkg/hosted/registry/npm"
	"gitlab.com/oss-cloud/registry/pkg/hosted/registry/pypi"

	"github.com/google/wire"

	"gitlab.com/oss-cloud/registry/pkg/common/logging"
	"gitlab.com/oss-cloud/registry/pkg/common/upstream"
	"gitlab.com/oss-cloud/registry/pkg/remote"
)

func NewHandler() http.Handler {
	wire.Build(
		wire.Value(logging.RootLogger),
		mux.NewHandler,
		newRouter,
		registry.NewMiddleware,
		remote.WireSet,
		npm.NewMounter,
		upstream.NewClient,
		rest.NewMounter,
		maven2.NewMounter,
		pypi.NewMounter,
	)
	return nil
}
