package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"go.uber.org/zap"

	"gitlab.com/oss-cloud/registry/pkg/common/logging"
	"gitlab.com/oss-cloud/registry/pkg/recursive/mux"
)

func main() {
	defer logging.RootLogger.Sync()
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	addr := ":" + port

	srv := &http.Server{
		Addr:         addr,
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      http.NotFoundHandler(),
	}

	handler, err := mux.NewHandler()
	if err != nil {
		panic(err)
	}
	srv.Handler = handler

	go func() {
		logging.RootLogger.Info("start listening", zap.String("addr", addr))
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			logging.RootLogger.Error("listen failed", zap.Error(err))
		}
	}()

	shutdownCh := make(chan os.Signal, 1)
	signal.Notify(shutdownCh, os.Interrupt, syscall.SIGTERM, syscall.SIGKILL)
	<-shutdownCh

	logging.RootLogger.Info("started to shutdown")

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		logging.RootLogger.Error("failed to shutdown", zap.Error(err))
		os.Exit(1)
	}
	os.Exit(0)
}
