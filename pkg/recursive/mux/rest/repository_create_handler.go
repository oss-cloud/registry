package rest

import (
	"encoding/json"
	"net/http"

	"gitlab.com/oss-cloud/registry/pkg/common/logging"
	"gitlab.com/oss-cloud/registry/pkg/recursive/core"
	"go.uber.org/zap"
)

type repositoryCreateHandler struct {
	Service core.RepositoryService
}

func (h *repositoryCreateHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var input core.RepositoryInput
	if err := json.NewDecoder(r.Body).Decode(&input); err != nil {
		w.WriteHeader(http.StatusUnprocessableEntity)
		return
	}

	repo, err := h.Service.Create(r.Context(), input)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")

	if err = json.NewEncoder(w).Encode(map[string]interface{}{"data": repo}); err != nil {
		logging.RootLogger.
			Named("mux/rest/repositoryCreateHandler").
			Error("failed to encode", zap.Error(err))
	}
}
