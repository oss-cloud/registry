package maven2

import (
	"io"
	"net/http"
	"strings"

	spec "github.com/opencontainers/image-spec/specs-go/v1"

	"github.com/gorilla/mux"
	"go.uber.org/zap"

	"gitlab.com/oss-cloud/registry/pkg/common/logging"
	"gitlab.com/oss-cloud/registry/pkg/hosted/registry/maven2/internal/repository"
	"gitlab.com/oss-cloud/registry/pkg/hosted/registry/maven2/internal/storage"
	"gitlab.com/oss-cloud/registry/pkg/remote"
)

var _ http.Handler = &deployFileHashHandler{}

type deployFileHashHandler struct {
	Remote *remote.Remote
	logger *zap.Logger
}

func newDeployFileHashHandler(remote *remote.Remote) *deployFileHashHandler {
	return &deployFileHashHandler{
		Remote: remote,
		logger: logging.RootLogger.Named("registry/maven/deployFileHandler"),
	}
}

func (h *deployFileHashHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	path := r.Context().Value(mavenPathContextKey).(repository.MavenPath)
	registryName := storage.ToRegistryName(vars["repository"], path.Coordinate)

	version := path.Coordinate.Version
	if version == "" {
		version = "index"
	}

	logger := h.logger.With(
		zap.String("registryName", registryName),
		zap.String("version", version),
	)

	manifest, err := h.Remote.FetchManifest(r.Context(), registryName, version)
	if err != nil {
		logger.Error("error when fetch manifest", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if manifest == nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	var descriptor *spec.Descriptor
	if version == "index" {
		descriptor = &manifest.Layers[0]
	} else {
		descriptor = remote.FindFile(manifest.Layers, strings.TrimSuffix(path.Filename, "."+path.HashType.String()))
		if descriptor == nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}
	}

	content, err := io.ReadAll(r.Body)
	if err != nil {
		logger.Error("error when reading body", zap.Error(err))
		return
	}

	if descriptor.Annotations[path.HashType.String()] == string(content) {
		w.WriteHeader(http.StatusOK)
		return
	}
	logger.Warn("invalid hash")
	w.WriteHeader(http.StatusExpectationFailed)
}
