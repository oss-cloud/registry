//go:build wireinject
// +build wireinject

package rest

import (
	"github.com/google/wire"
	"gitlab.com/oss-cloud/registry/pkg/recursive/core"
)

func NewMount(
	core.RepositoryService,
) Mount {
	wire.Build(
		newMount,
		wire.Struct(new(repositoryListHandler), "*"),
		wire.Struct(new(repositoryCreateHandler), "*"),
	)
	return nil
}
