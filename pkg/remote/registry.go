package remote

import "os"

type Options struct {
	Insecure  bool
	PlainHTTP bool

	Host   string
	Prefix string
}

func OptionsFromEnv() *Options {
	return &Options{
		Host:      os.Getenv("REMOTE_REGISTRY"),
		Prefix:    os.Getenv("REMOTE_PREFIX"),
		Insecure:  os.Getenv("REMOTE_INSECURE") == "1",
		PlainHTTP: os.Getenv("REMOTE_PLAIN_HTTP") == "1",
	}
}
