package maven2

import (
	"net/http"
	"strings"

	"gitlab.com/oss-cloud/registry/pkg/hosted/registry"
	"gitlab.com/oss-cloud/registry/pkg/hosted/registry/common"

	"gitlab.com/oss-cloud/registry/pkg/common/checksum"

	"github.com/gorilla/mux"
)

type Mounter func(router *mux.Router)

func newMounter(
	fetchMetaHandler *fetchMetaHandler,
	uploadBlobHandler *deployFileHandler,
	deployFileHashHandler *deployFileHashHandler,
	deployPomFileHandler *deployPomFileHandler,
	deployMetaFileHandler *deployMetaFileHandler,
	fetchMetaFileHandler *fetchMetaFileHandler,
	fetchFileHandler *fetchFileHandler,
	fetchFileHashHandler *fetchFileHashHandler,
) Mounter {
	var isHash mux.MatcherFunc = func(r *http.Request, _ *mux.RouteMatch) bool {
		for _, ht := range checksum.HashTypeValues {
			if strings.HasSuffix(r.URL.Path, "."+ht.String()) {
				return true
			}
		}
		return false
	}

	return func(router *mux.Router) {
		router.Use(common.CreateEnabledMiddleware(func(config *registry.Config) bool {
			return config.Registry.Maven.Enabled
		}))
		router.Use(mavenPathMiddleware)

		// POM file
		router.Methods(http.MethodGet).MatcherFunc(pathSuffix(".pom")).Handler(fetchMetaHandler)
		router.Methods(http.MethodPut, http.MethodPost).MatcherFunc(pathSuffix(".pom")).Handler(deployPomFileHandler)

		// binary file
		router.Methods(http.MethodPut, http.MethodPost).MatcherFunc(pathSuffix(".jar")).Handler(uploadBlobHandler)
		router.Methods(http.MethodGet).MatcherFunc(pathSuffix(".jar")).Handler(fetchFileHandler)

		// Hash
		router.Methods(http.MethodPut, http.MethodPost).MatcherFunc(isHash).Handler(deployFileHashHandler)
		router.Methods(http.MethodGet).MatcherFunc(isHash).Handler(fetchFileHashHandler)

		// maven-metadata.xml
		router.Methods(http.MethodPut, http.MethodPost).MatcherFunc(pathSuffix("maven-metadata.xml")).Handler(deployMetaFileHandler)
		router.Methods(http.MethodGet).MatcherFunc(pathSuffix("maven-metadata.xml")).Handler(fetchMetaFileHandler)
	}
}
