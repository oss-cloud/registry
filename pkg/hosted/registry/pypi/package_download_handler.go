package pypi

import (
	"fmt"
	"net/http"

	"gitlab.com/oss-cloud/registry/pkg/common/logging"

	"gitlab.com/oss-cloud/registry/pkg/common/ioutil"

	"github.com/gorilla/mux"
	"github.com/opencontainers/go-digest"
	"go.uber.org/zap"

	"gitlab.com/oss-cloud/registry/pkg/remote"
)

type packageDownloadHandler struct {
	Remote *remote.Remote
	logger *zap.Logger
}

func newPackageDownloadHandler(remote *remote.Remote) *packageDownloadHandler {
	return &packageDownloadHandler{
		Remote: remote,
		logger: logging.RootLogger.Named("registry/pypi/packageDownloadHandler"),
	}
}

func (h *packageDownloadHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	name := fmt.Sprintf("%s/pypi/%s", vars["repository"], vars["package"])
	dig := digest.Digest(vars["digest"])
	body, err := h.Remote.FetchBlob(r.Context(), name, dig)
	if err != nil {
		h.logger.Error(
			"failed to fetch blob",
			zap.Error(err),
			zap.String("name", name),
			zap.Stringer("digest", dig),
		)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	header := w.Header()
	header.Set("Content-Type", "binary/octant-stream")
	header.Set("Etag", "\""+dig.String()+"\"")
	if _, err = ioutil.CopyAndClose(w, body); err != nil {
		h.logger.Error(
			"failed to copy to response",
			zap.Error(err),
			zap.String("name", name),
			zap.Stringer("digest", dig),
		)
	}
}
