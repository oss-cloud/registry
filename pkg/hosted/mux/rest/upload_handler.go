package rest

import (
	"encoding/json"
	"io"
	"net/http"

	"gitlab.com/oss-cloud/registry/pkg/remote"
)

type uploadHandler struct {
	Remote *remote.Remote
}

func (h *uploadHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	descriptor, err := h.Remote.PushBlob(r.Context(), r.URL.Query().Get("name"), io.NopCloser(r.Body))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(descriptor)
}
