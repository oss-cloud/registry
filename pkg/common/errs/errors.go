package errs

import "errors"

var (
	ErrUnAuthorized     = errors.New("response: unauthorized")
	ErrForbidden        = errors.New("response: forbidden")
	ErrUnexpectedStatus = errors.New("response: unexpected status")
	ErrNotFound         = errors.New("response: not found")
)
