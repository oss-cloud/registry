package pooled

import (
	"bytes"
	"sync"
)

var Buffer = sync.Pool{
	New: func() any {
		return new(bytes.Buffer)
	},
}
