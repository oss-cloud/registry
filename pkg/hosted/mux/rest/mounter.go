package rest

import (
	"net/http"

	"gitlab.com/oss-cloud/registry/pkg/hosted/middlewares/authorization"

	"github.com/gorilla/mux"
)

type Mounter func(router *mux.Router)

func newMounter(
	uploadHandler *uploadHandler,
) Mounter {
	return func(router *mux.Router) {
		router.Use(authorization.Middleware)
		router.Methods(http.MethodPut).Path("/blob").Handler(uploadHandler)
	}
}
