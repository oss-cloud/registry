package remote

import (
	"runtime"

	"go.uber.org/zap"

	"gitlab.com/oss-cloud/registry/pkg/common/context"
	"gitlab.com/oss-cloud/registry/pkg/common/logging"
)

type UploadWorker struct {
	ch chan *StoreManifestConfigRequest

	logger *zap.Logger
	Client *Client
}

func NewUploadWorker(client *Client) *UploadWorker {
	return &UploadWorker{
		ch:     make(chan *StoreManifestConfigRequest, runtime.NumCPU()),
		logger: logging.RootLogger.Named("remote/uploadWorker"),
		Client: client,
	}
}

func (w *UploadWorker) Submit(req *StoreManifestConfigRequest) {
	w.ch <- req
}

func (w *UploadWorker) Start(ctx context.Context) {
	for {
		select {
		case req := <-w.ch:
			if err := w.Client.StoreAsManifestConfig(req); err != nil {
				w.logger.Error(
					"fail to store manifest",
					zap.String("registryName", req.RegistryName),
					zap.String("ref", req.Ref),
				)
			}
		case <-ctx.Done():
			return
		}
	}
}
