package logging

import (
	"os"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var RootLogger *zap.Logger

func init() {
	var err error
	config := zap.NewDevelopmentConfig()
	level := os.Getenv("LOG_LEVEL")
	if level == "" {
		level = "debug"
	}
	config.Level, err = zap.ParseAtomicLevel(level)
	if err != nil {
		config.Level = zap.NewAtomicLevelAt(zapcore.DebugLevel)
	}

	RootLogger, err = config.Build()
	if err != nil {
		panic(err)
	}
}
