//go:build wireinject
// +build wireinject

package rest

import (
	"github.com/google/wire"

	"gitlab.com/oss-cloud/registry/pkg/remote"
)

func NewMounter(*remote.Remote) Mounter {
	wire.Build(
		newMounter,
		wire.Struct(new(uploadHandler), "*"),
	)
	return nil
}
