package mux

import (
	"github.com/gorilla/mux"
	"gitlab.com/oss-cloud/registry/pkg/recursive/mux/rest"
)

func newRouter(
	rest rest.Mount,
) *mux.Router {
	m := mux.NewRouter()

	rest(m.PathPrefix("/api").Subrouter())

	return m
}
