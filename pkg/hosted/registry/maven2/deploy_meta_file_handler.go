package maven2

import (
	"io"
	"net/http"

	"gitlab.com/oss-cloud/registry/pkg/hosted/registry/maven2/internal/repository"
	"gitlab.com/oss-cloud/registry/pkg/hosted/registry/maven2/internal/storage"

	"gitlab.com/oss-cloud/registry/pkg/common/logging"

	"github.com/gorilla/mux"
	spec "github.com/opencontainers/image-spec/specs-go/v1"
	"go.uber.org/zap"

	"gitlab.com/oss-cloud/registry/pkg/remote"
)

var _ http.Handler = &deployMetaFileHandler{}

type deployMetaFileHandler struct {
	Client *remote.Client
	logger *zap.Logger
}

func newDeployMetaFileHandler(client *remote.Client) *deployMetaFileHandler {
	return &deployMetaFileHandler{
		Client: client,
		logger: logging.RootLogger.Named("registry/maven/deployMetaFileHandler"),
	}
}

func (h *deployMetaFileHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	const ref = "index"

	vars := mux.Vars(r)
	path := r.Context().Value(mavenPathContextKey).(repository.MavenPath)
	registryName := storage.ToRegistryName(vars["repository"], path.Coordinate)

	logger := h.logger.With(
		zap.String("registryName", registryName),
		zap.String("version", path.Coordinate.Version),
	)

	manifest, err := h.Client.FetchManifest(r.Context(), registryName, ref)
	if err != nil {
		logger.Error("failed to fetch manifest", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if manifest == nil {
		manifest = &spec.Manifest{
			MediaType: spec.MediaTypeImageManifest,
		}
	}

	descriptor, err := h.Client.PushBlob(r.Context(), registryName, io.NopCloser(r.Body))
	if err != nil {
		logger.Error("failed to push blob", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	descriptor.MediaType = "text/xml"

	manifest.Layers = []spec.Descriptor{*descriptor}

	if manifest.Config.Size == 0 {
		config, err := h.Client.CreateEmptyBlob(r.Context(), registryName)
		if err != nil {
			h.logger.Error(
				"failed to create placeholder config",
				zap.Error(err),
				zap.String("name", registryName),
			)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		config.MediaType = "application/vnd.oss-cloud.registry.maven+json"
		manifest.Config = *config
	}

	if err = h.Client.PushManifest(r.Context(), registryName, ref, manifest); err != nil {
		logger.Error(
			"failed to pushing manifest",
			zap.Error(err),
		)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}
