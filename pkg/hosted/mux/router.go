package mux

import (
	"net/http"

	"gitlab.com/oss-cloud/registry/pkg/hosted/middlewares/authorization"
	"gitlab.com/oss-cloud/registry/pkg/hosted/middlewares/registry"
	"gitlab.com/oss-cloud/registry/pkg/hosted/mux/rest"
	"gitlab.com/oss-cloud/registry/pkg/hosted/registry/maven2"
	"gitlab.com/oss-cloud/registry/pkg/hosted/registry/npm"
	"gitlab.com/oss-cloud/registry/pkg/hosted/registry/pypi"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func newRouter(
	rm *registry.Middleware,
	npm npm.Mounter,
	maven maven2.Mounter,
	pypi pypi.Mounter,
	rest rest.Mounter,
) *mux.Router {
	r := mux.NewRouter()

	r.Path("/metrics").Methods(http.MethodGet).Handler(promhttp.Handler())
	rest(r.PathPrefix("/api/v0").Subrouter())

	registryRouter := r.PathPrefix("/registry/{repository}").Subrouter()
	registryRouter.Use(authorization.Middleware, rm.Middleware)
	{
		npmRouter := registryRouter.PathPrefix("/npm").Subrouter()
		npm(npmRouter)
	}

	maven(registryRouter.PathPrefix("/maven").Subrouter())
	pypi(registryRouter.PathPrefix("/pypi").Subrouter())

	return r
}
