package remote

import "testing"

func TestNewTlsConfig(t *testing.T) {
	config := newTlsConfig(&Options{Insecure: true})
	if !config.InsecureSkipVerify {
		t.Errorf("expect to be true, got false")
	}
}
