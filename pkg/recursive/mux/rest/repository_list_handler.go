package rest

import (
	"encoding/json"
	"net/http"

	"go.uber.org/zap"

	"gitlab.com/oss-cloud/registry/pkg/common/logging"
	"gitlab.com/oss-cloud/registry/pkg/recursive/core"
)

var _ http.Handler = &repositoryListHandler{}

type repositoryListHandler struct {
	Service core.RepositoryService
}

func (h *repositoryListHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	repositories, err := h.Service.List(r.Context())
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	responseBody := map[string]interface{}{
		"data": repositories,
	}

	w.Header().Set("Content-Type", "application/json")

	if err = json.NewEncoder(w).Encode(responseBody); err != nil {
		logging.RootLogger.
			Named("mux/rest/repositoryListHandler").
			Error("failed to encode", zap.Error(err))
	}
}
