package repository

import (
	"errors"
)

var ErrUnknownAlgo = errors.New("maven/repository: unknown algo")

type SignatureType string

var SignatureTypeValues = []SignatureType{SignatureTypeAsc}

const (
	SignatureTypeAsc SignatureType = "asc"
)

func (s SignatureType) String() string {
	return string(s)
}
