//go:build wireinject
// +build wireinject

package postgres

import (
	"database/sql"

	"github.com/google/wire"

	"gitlab.com/oss-cloud/registry/pkg/recursive/core"
)

func NewRepositoryService(*sql.DB) core.RepositoryService {
	wire.Build(
		newRepositoryService,
		wire.Bind(new(core.RepositoryService), new(*repositoryService)),
	)
	return nil
}
