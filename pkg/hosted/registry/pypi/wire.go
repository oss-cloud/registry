//go:build wireinject
// +build wireinject

package pypi

import (
	"github.com/google/wire"
	"gitlab.com/oss-cloud/registry/pkg/remote"
)

func NewMounter(
	*remote.Remote,
) Mounter {
	wire.Build(
		newMounter,
		newPackageVersionListHandler,
		newPackageDownloadHandler,
	)
	return nil
}
