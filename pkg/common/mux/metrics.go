package mux

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func metricsHandler(next http.Handler) http.Handler {
	promHandler := promhttp.Handler()
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/metrics" {
			promHandler.ServeHTTP(w, r)
			return
		}
		next.ServeHTTP(w, r)
	})
}
