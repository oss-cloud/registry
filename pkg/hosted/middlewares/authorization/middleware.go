package authorization

import (
	"net/http"

	"gitlab.com/oss-cloud/registry/pkg/common/context"
	"gitlab.com/oss-cloud/registry/pkg/hosted/registry"
)

var _ http.Handler = &authorizationHandler{}

func Middleware(next http.Handler) http.Handler {
	return &authorizationHandler{next}
}

type authorizationHandler struct {
	next http.Handler
}

func (h *authorizationHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	authorization := r.Header.Get("Authorization")
	if authorization == "" {
		h.next.ServeHTTP(w, r)
		return
	}
	ctx := context.WithValue(r.Context(), registry.AuthorizationContextKey, r.Header.Get("Authorization"))
	h.next.ServeHTTP(w, r.WithContext(ctx))
}
