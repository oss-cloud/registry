//go:build wireinject
// +build wireinject

package npm

import (
	"net/http"

	"gitlab.com/oss-cloud/registry/pkg/common/upstream"

	"gitlab.com/oss-cloud/registry/pkg/common/logging"

	"github.com/google/wire"

	"gitlab.com/oss-cloud/registry/pkg/remote"
)

func NewMounter(
	*http.Client,
	*upstream.Client,
	*remote.Client,
	*remote.Remote,
) Mounter {
	wire.Build(
		newMounter,
		newPackageFetchHandler,
		wire.Value(logging.RootLogger),
	)
	return nil
}
