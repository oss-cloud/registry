package mux

import (
	"net/http"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"gitlab.com/oss-cloud/registry/pkg/common/logging"
	"gitlab.com/oss-cloud/registry/pkg/common/mux/middlewares/metrrics"
)

func newHandler(r *mux.Router) (h http.Handler) {
	r.Path("/metrics").Methods(http.MethodGet).Handler(promhttp.Handler())

	h = r

	h = metrrics.Middleware(h)

	h = handlers.RecoveryHandler(
		handlers.PrintRecoveryStack(true),
		handlers.RecoveryLogger(&recoverWriter{logging.RootLogger.Named("recovery").Sugar()}),
	)(h)
	h = handlers.CombinedLoggingHandler(
		&requestWriter{logging.RootLogger.Named("request-log")},
		h,
	)
	h = handlers.ProxyHeaders(h)
	h = metricsHandler(h)
	return h
}
