package repository

import (
	"fmt"
	"testing"

	"gitlab.com/oss-cloud/registry/pkg/common/checksum"
)

func TestNewPath(t *testing.T) {
	rawPath := "/org/slf4j/slf4j-api/1.7.6/slf4j-api-1.7.6.pom.sha256"
	path := newPath(rawPath, Coordinate{})
	if path.Path != rawPath {
		t.Error("invalid raw path")
	}
	if path.Filename != "slf4j-api-1.7.6.pom.sha256" {
		t.Errorf("invalid filename, got %q", path.Filename)
	}
	if path.HashType != checksum.HashTypeSHA256 {
		t.Errorf("invalid hash type, got : %q", path.HashType)
	}
}

func TestParseMavenPath(t *testing.T) {
	for index, tc := range []struct {
		path     string
		filename string
		hashType checksum.HashType
	}{
		{
			path:     "/org/slf4j/slf4j-api/1.7.36/slf4j-api-1.7.36.pom",
			filename: "slf4j-api-1.7.36.pom",
		},
		{
			path:     "/org/slf4j/slf4j-api/maven-metadata.xml",
			filename: "maven-metadata.xml",
		},
		{
			path:     "/org/slf4j/slf4j-api/maven-metadata.xml.md5",
			filename: "maven-metadata.xml.md5",
			hashType: checksum.HashTypeMD5,
		},
	} {
		index, tc := index, tc
		t.Run(fmt.Sprintf("case-%d", index+1), func(t *testing.T) {
			t.Parallel()
			mavenPath := ParseMavenPath(tc.path)
			if mavenPath.Filename != tc.filename {
				t.Errorf("expect filename to be %q, got %q", tc.filename, mavenPath.Filename)
			}
			if mavenPath.HashType != tc.hashType {
				t.Errorf("expect hashType to be %q, got %q", tc.hashType, mavenPath.HashType)
			}
		})
	}
}
