package remote

import (
	"fmt"
	"io"
	"strings"
	"sync"

	spec "github.com/opencontainers/image-spec/specs-go/v1"
	"go.uber.org/multierr"
	"go.uber.org/zap"

	"gitlab.com/oss-cloud/registry/pkg/common/checksum"
	"gitlab.com/oss-cloud/registry/pkg/common/context"
	"gitlab.com/oss-cloud/registry/pkg/common/ioutil"
	"gitlab.com/oss-cloud/registry/pkg/common/logging"
)

type Client struct {
	*Remote
	logger *zap.Logger
}

func NewRemoteClient(remote *Remote) *Client {
	return &Client{
		Remote: remote,
		logger: logging.RootLogger.Named("remote/client"),
	}
}

func (c *Client) PushBlob(ctx context.Context, name string, content io.ReadCloser) (_ *spec.Descriptor, err error) {
	buffer, err := ioutil.NewBufferFromReadCloser(content)
	if err != nil {
		return
	}

	descriptor, err := c.Remote.PushBlob(ctx, name, buffer.Reader())
	if err != nil {
		return
	}

	if descriptor.Annotations == nil {
		descriptor.Annotations = make(map[string]string)
	}

	for _, hashType := range checksum.HashTypeValues {
		digester := hashType.Hash()
		if _, err := ioutil.CopyAndClose(digester, buffer.Reader()); err == nil {
			descriptor.Annotations[hashType.String()] = fmt.Sprintf("%x", digester.Sum(nil))
		}
	}

	return descriptor, nil
}

func (c *Client) CreateEmptyBlob(ctx context.Context, name string) (*spec.Descriptor, error) {
	return c.PushBlob(ctx, name, io.NopCloser(strings.NewReader("{}")))
}

type StoreManifestConfigRequest struct {
	Context context.Context

	RegistryName string
	Ref          string

	Content io.ReadSeekCloser

	PostDescriptor func(descriptor *spec.Descriptor)
}

func (c *Client) StoreAsManifestConfig(request *StoreManifestConfigRequest) error {
	ctx := request.Context
	var wg sync.WaitGroup
	errCh := make(chan error, 2)
	var (
		manifest   *spec.Manifest
		descriptor *spec.Descriptor
	)
	wg.Add(2)
	go func() {
		defer wg.Done()
		var err error
		descriptor, err = c.Remote.PushBlob(ctx, request.RegistryName, request.Content)
		if err != nil {
			c.logger.Error(
				"error when pushing blob",
				zap.String("registryName", request.RegistryName),
				zap.Error(err),
			)
			errCh <- err
			return
		}
		request.PostDescriptor(descriptor)
	}()

	go func() {
		defer wg.Done()
		var err error
		manifest, err = c.Remote.FetchManifest(ctx, request.RegistryName, request.Ref)
		if err != nil {
			c.logger.Error(
				"error when fetching manifest",
				zap.String("registryName", request.RegistryName),
				zap.Error(err),
			)
			errCh <- err
			return
		}
		if manifest == nil {
			manifest = &spec.Manifest{
				MediaType: spec.MediaTypeImageManifest,
			}
		}
	}()

	go func() {
		wg.Wait()
		close(errCh)
	}()
	var multiErr error
loop:
	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case err, done := <-errCh:
			if err != nil {
				multiErr = multierr.Append(multiErr, err)
			}
			if done {
				break loop
			}
		}
	}
	if multiErr != nil {
		return multiErr
	}

	manifest.Config = *descriptor

	if err := c.Remote.PushManifest(ctx, request.RegistryName, request.Ref, manifest); err != nil {
		c.logger.Error(
			"error occur when push manifest",
			zap.Error(err),
			zap.String("registryName", request.RegistryName),
			zap.String("ref", request.Ref),
		)
		return err
	}
	return nil
}
