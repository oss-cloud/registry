package remote

import "testing"

func TestRemoteSchema(t *testing.T) {
	remote := Remote{
		Options: &Options{
			PlainHTTP: true,
		},
	}
	if scheme := remote.scheme(); scheme != "http" {
		t.Errorf("expect http, got %q", scheme)
	}
}
